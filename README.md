
## PyPDF2

PyPDF2 is a pure-python PDF library capable of
splitting, merging together, cropping, and transforming
the pages of PDF files. It can also add custom
data, viewing options, and passwords to PDF files.
It can retrieve text and metadata from PDFs as well
as merge entire files together.


## Custom Changes

During the splitting main pdf into multiple pdf we have faced some errors and those errors are related to the pypdf2 source code, Due to this we have forked the existing repository and added fixes to our repository.

Reference links
https://github.com/47billion-gagandeep/PyPDF2/tree/master
https://github.com/mstamy2/PyPDF2


